Installation instructions:
  Download and install the module
  Create a hook in your theme or custom module to define the forms you want to protect (see honeypot_submit.api.php)
  Clear caches
  Inspect source of forms to verify there are multiple submit buttons

