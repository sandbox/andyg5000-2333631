<?php

/**
 * @file
 * Provides developer documentation for using exposed hooks.
 */

/**
 * Implements hook_honeypot_submit_form_ids_alter().
 *
 * Append the form's ID to the array of $form_ids to enable honeypot submit
 * on them.
 */
function MYMODULE_honeypot_submit_form_ids_alter(&$form_ids) {
  // Protect all user forms.
  $form_ids[] = 'user_login';
  $form_ids[] = 'user_pass';
  $form_ids[] = 'user_register_form';
  // Protect a webform.
  $form_ids[] = 'webform_client_form_283';
}
